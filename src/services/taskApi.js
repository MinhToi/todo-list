import axios from './axiosConfig';

const getAllTasks = () => {
  let endPoint = '/tasks';
  return axios.get(endPoint);
};
const removeTask = (id) => {
  let endPoint = `/tasks/${id}`;
  return axios.delete(endPoint);
};
const updateTask = (updatedTask, id) => {
  let endPoint = `/tasks/${id}`;
  return axios.post(endPoint, updatedTask);
};
const addTask = (newTask) => {
  let endPoint = '/tasks';
  return axios.post(endPoint, newTask);
};
export const taskApi = {
  getAllTasks,
  removeTask,
  updateTask,
  addTask,
};
