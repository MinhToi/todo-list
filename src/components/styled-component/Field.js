import styled from 'styled-components';

export const FieldStyled = styled.input`
  border-radius: 5px;
  margin: 5px;
  height: 25px;
  width: 55vw;
`;

export const FormStyled = styled.form`
  display: ${(props) => (props.inline ? 'inline-block' : 'block')};
`;
