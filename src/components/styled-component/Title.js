import styled from 'styled-components';

export const Title = styled.h3`
  font-size: 24px;
  text-align: center;
`;
export const SubTitle = styled.h3`
  font-size: 16px;
`;
