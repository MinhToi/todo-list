import styled from 'styled-components';

export const ButtonStyled = styled.button`
  height: 30px;
  font-size: 1em;
  margin: 1em;
  padding: 0.25em 1em;
  border-radius: 3px;
  border: ${(props) => (props.primary ? 'none' : '2px solid black')};
  background-color: ${(props) => (props.primary ? 'blue' : 'white')};
  color: ${(props) => (props.primary ? 'white' : 'black')};
  display: ${(props) => (props.inline ? 'inline-block' : 'block')}

  &: hover {
    background-color: #f3faff;
  }
`;
