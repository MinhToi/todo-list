import styled from 'styled-components';

export const LayoutCenter = styled.div`
  max-width: 80vw;
  margin: auto;
  border: 2px solid black;
  text-align: center;
  & > div {
    display: inline-block;
  }
`;
