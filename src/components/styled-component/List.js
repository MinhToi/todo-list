import styled from 'styled-components';

export const List = styled.ul`
  list-style-type: none;
  padding: 0;
`;

export const ListItem = styled.li`
  width: 60vw;
  padding: 0 10px 0 10px;
  margin-bottom: 10px;
  border-radius: 5px;
  border: 2px solid black;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
