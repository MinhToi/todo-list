import React, { useEffect, useState, useRef } from 'react';
import { ButtonStyled } from './../styled-component/Button';
import { FieldStyled, FormStyled } from './../styled-component/Field';
import { List, ListItem } from './../styled-component/List';
import { Title, SubTitle } from './../styled-component/Title';
import { LayoutCenter } from './../styled-component/LayoutCenter';

import { taskApi } from './../../services/taskApi';

function Index() {
  //COMPUTED LOGIC
  const [listTasks, setListTasks] = useState();
  let inputNewTask = useRef();

  //Implement something when component was assigned
  useEffect(() => {
    fetchAllTask();
  }, []);

  //METHOD
  //fetch all task through api
  const fetchAllTask = () => {
    taskApi
      .getAllTasks()
      .then((res) => setListTasks(res.data))
      .catch((err) => console.log('Error: ', err));
  };

  //handle event adding a new task
  const handleAddNewTask = () => {
    let body = { title: inputNewTask.current.value };
    taskApi
      .addTask(body)
      .then(() => fetchAllTask())
      .catch((err) => console.log('error adding: ', err));
  };
  //handle event removing a task
  const handleRemoveTask = (id) => {
    taskApi
      .removeTask(id)
      .then(() => fetchAllTask())
      .catch((err) => console.log('error remove task', err));
  };
  console.log('render -', listTasks);

  //DOM
  return (
    <LayoutCenter>
      <div>
        <Title>Todo list</Title>
        <ButtonStyled onClick={handleAddNewTask} inline>
          Add
        </ButtonStyled>
        <FormStyled inline>
          <FieldStyled ref={inputNewTask} />
        </FormStyled>
      </div>
      <div>
        <List>
          {!!listTasks ? (
            listTasks.map((item, index) => (
              <ListItem key={index}>
                <SubTitle>{item.title}</SubTitle>
                <ButtonStyled primary onClick={() => handleRemoveTask(item.id)}>
                  Done
                </ButtonStyled>
              </ListItem>
            ))
          ) : (
            <p>Loading</p>
          )}
        </List>
      </div>
    </LayoutCenter>
  );
}

export default Index;
